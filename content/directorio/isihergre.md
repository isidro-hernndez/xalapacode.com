+++
name = "Isidro Hernandez Gregorio"
nickname = "isihergre"
description = "Programador, Desarrollador Web, BBoy, Fotografo, amante y contribuidor de la cultura hip hop"
skills = "php, js, python, css, html, linux, sql ,fotografia"
thumbnail = "/img/directorio/isihergre.jpg"
draft = false
date = "2019-01-01T19:37:18-06:00"
disponibility = "Remoto || Home Oficce"
rol = "veterano"
website = "https://isidro.dev"
instagram = "@unfotografodexalapa"

author = "isihergre"
+++

Programador apasionado, fotógrafo de oficio, bboy en formación, contribuidor y amante de la cultura hip hop.

**Formación**

Licenciatura en Tecnologias Computacionales en la Universidad Veracruzana

**Acerca de mí**

Programador enfocado a desarrollo web principalmente, estudiante de: paradigmas, arquitecturas, patrones de diseño,  buenas practicas y todo lo que tenga que ver con el buen desarrollo de software.

**Formación laboral**

Eh trabajado informalmente de: Mesero, bar tender, cajero, repartidor de comida, empleado de mostrador, limpieza profunda, serigrafista, barrendero, cargador y principalmente fotógrafo y video filmador.
Empecé a cobrar por desarrollo de software desde el 2016 contribuyendo a pequeños proyectos en modo freelance, trabaje para una empresa de desarrollo de aplicaciones móviles realizando backend para las mismas, estuve en gobierno del estado de Veracruz en la secretaria de bienestar, y actualmente trabajo para una empresa como desarrollador de su sistema de planificación de recursos(ERP).

**Proyectos personales**

Algunos proyectos propios en los que estoy trabajando.

* Open Hip Hop(Aún en desarrollo).
* Mi Garnacha(Aún en desarrollo).
* LaCuponera(Aún en desarrollo).

**Hobbies**

Tomar fotografías, aprender cosas nuevas(desde como hacer unos chilaquiles hasta como salvarle la vida a una persona con tan solo presionar un botón), bailar breakin', asistir a eventos culturales, jugar LOL entre otras cosas.

