+++
title = "Meetup 9 de Agosto 2019"
date = "2019-07-27T17:38:00-06:00"
event_date = "2019-08-09T18:00:00-06:00"
event_place = "Colegio de Arquitectos"
description = "El evento de agosto, de regreso al colegio de arquitectos"
thumbnail = "/img/eventos/meetup-2019-08-09.png"
author = "categulario"
draft = false
turnout = 18
+++

## Las platicas

1. **Un poco de accesibilidad para la web**, por Iván Mejía
  - [Video de la presentación](https://www.youtube.com/watch?v=VR0dn-OYCMQ)
  - [Archivo PDF de la presentación](https://xalapacode.categulario.tk/archive/2019/08/consejos%20de%20accesibilidad.pdf)
2. **Aplicando las técnicas de Dale Carnegie**, por José Andrés Pérez Grajales
  - [Video de la presentación](https://www.youtube.com/watch?v=1tmVwtQBCAI)
  - [Archivo PDF de la presentación](https://xalapacode.categulario.tk/archive/2019/08/Co%CC%81mo%20ganar%20amigos%20e%20influir%20sobre%20las%20personas.pdf)
3. **¿Cómo hacer composta en casa?**, por Teresa Cepero
  - [Archivo PDF de la presentación](https://xalapacode.categulario.tk/archive/2019/08/Co%CC%81mo%20hacer%20composta%20en%20casa.pdf)
4. **Instalación de Debian a través de red**, por Gerardo Contreras
  - [Archivo PDF de la presentación](https://xalapacode.categulario.tk/archive/2019/08/instalacionDebianDesdeTFTP.pdf)

También puedes consultar [el playlist completo en youtube](https://www.youtube.com/watch?v=VR0dn-OYCMQ&list=PLy5vbEYJmUWyBnwXVvRI26l_okpzEcAwt).

## Sobre la sede

El Colegio de arquitectos nos recibe por segunda ocasión. Ubicado en Av. de la República esquina con Leyes de reforma en la colonia Esther Badillo. A continuación puedes encontrar el mapa.

[Mapa](https://www.google.com/maps/place/Colegio+de+Arquitectos/@19.532607,-96.9013797,17z/data=!3m1!4b1!4m5!3m4!1s0x85db318aabc73c4d:0xdad99096fc59c101!8m2!3d19.532607!4d-96.899191)

## Sobre el evento

Los meetups de xalapacode son encuentros en los que se realizan una serie de desconferencias de 15 minutos de duración por miembros de la comunidad como tu sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
