+++
title = "Charla Debian: Al infinito y más Debian"
date = "2019-06-18T12:00:00-06:00"
event_date = "2019-06-22T11:00:00-06:00"
event_place = "CIDI FEIUV"
description = "Una charla para conmemorar el cumpleaños de Debian"
thumbnail = "/img/eventos/talk-2019-06-22-debian.jpg"
author = "jail"
turnout = 24
draft = false
+++

Acompáñennos a celebrar el 23 aniversario del lanzamiento de la primera versión estable de Debian con esta charla magistral impartida por Gerardo Contreras este sábado 22 de Junio a las 11:00 hrs en el CIDI de la FEIUV.

## Sobre la sede

El Centro de Investigación y Desarrollo Informático de la Facultad de Estadistica e Informatica de la UV sera la sede de esta charla.

[Mapa](https://goo.gl/maps/jSVYkro2biDL7wD16)

## Sobre el evento

Este es un evento unico y conmemorativo para celebrar el lanzamiento de la primera versión estable de Debian el día 17 de Junio de 1996 [Enlace del anuncio](https://lists.debian.org/debian-announce/1996/msg00021.html) es gratuito y libre.
